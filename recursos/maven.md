# FAQ
1. ¿Cómo instalo maven?
- En Windows:

https://maven.apache.org/guides/getting-started/windows-prerequisites.html
- En linux puede ser con su gestor de paquetes.

1. ¿Cómo inicializo un proyecto?
```bash
mvn archetype:generate \
-DgroupId=fc \
-DartifactId=Proyecto1 \
-DarchetypeArtifactId=maven-archetype-quickstart \
-DinteractiveMode=false
```

2. ¿Cómo agrego *Jflex* a mi proyecto?
http://jflex.sourceforge.net/jflex-maven-plugin/usage.html



3. ¿Cómo compilo y ejecuto mi proyecto?

```bash
 mvn compile exec:java -Dexec.mainClass="fc.App"
```

Mas información acerca del plugin `exec`.
https://www.mojohaus.org/exec-maven-plugin/usage.html