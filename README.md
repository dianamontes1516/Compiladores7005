# Compiladores7005

Faculdad de Ciencias, UNAM.

Material para el laboratorio para un curso introductorio a Compiladores.
Partes:
* Análisis Léxico
* Análisis Sintáctico
* Análisis Dependiente del Contexto (o semántico)
* Generación de código (ensamblador para la arquitectura `MIPS`)

## Herramientas
* Java 1.8>=
* Jflex
* byacc

## Ayudante de laboratorio
Diana Olivia Montes Aguilar

Contacto
`niner90@ciencias.unam.mx`


## Criterios de Evaluación
* Las prácticas y proyecto serán en equipo de a lo más tres integrantes.
* La entrega de los trabajos será vía un repositorio de `git` por equipo.
* La calificación asignada a los proyectos será para cada miembro del equipo
  que demuestre haber participado. Se tomarán como pruebas:
  1. Commits con la autoría del integrante.
  2. Entrevista personal, presencial y fluida.
