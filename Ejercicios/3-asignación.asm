# Asignación
# @author Compiladores
.data

Entero:	        .word 1
# Cadena guarda una dirección dónde empieza la cadena
Cadena:	        .word 0
Booleano:	.word 0

.text
main:

.data
  strtmp0:   .asciiz "Foo"
.text
  la $a0, strtmp0
  sw $a0, Cadena

  li $v0, 4
  lw $a0, Cadena
  syscall


jr $ra
