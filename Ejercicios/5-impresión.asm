#
# Programa en MIPS que imprime
# @author LosBastardosDeTuring
#
main:
	.data
cad: 	.asciiz		"WTF\n"		# Guardamos la cadena
int:    .word   	5    		# Guardamos el entero
True:	.word		1		# Representacion del booleano true
False:	.word		0		# Representación del booleano False

	.text
	lw 	$a0, int		# Cargamos el entero
	li 	$v0, 1			# Codigo llamada al sistema para imprimir int (1)
	syscall
	lw	$a0, True		# Cargamos el contenido de la etiqueta `True`
	li	$v0, 1			# Codigo llamada al sistema para imprimir int (1)
	syscall
	la	$a0, cad		# Cargamos el inicio de la cadena
	li	$v0, 4			# Codigo llamada al sistema para imprimir cadena (5)
	syscall
end:    jr $ra
