# Código para ciclos while
# Producido con compilador de p
main:
.data
  i: .word 0
.text
while0:
  li  $t0, 0
  beq $t0 $zero endwhile0
  li  $t0, 3
  move $a0, $t0
  li $v0, 1
  syscall
  j while0
endwhile0:
  li  $t0, 2
  sw  $t0, i


while1:
  lw  $t1, i
  li  $t2, 2
  bne $t1, $t2, neq0
  li $t0, 1
  j endeq0
neq0:
  li $t0, 0
endeq0:
  beq $t0 $zero endwhile1
.data
 strtmp0:   .asciiz "Dos\n"
.text
  li $v0, 4
  la $a0, strtmp0
  syscall
  li  $t2, 1
  sw  $t2, i
  j while1
endwhile1:
end:    jr $ra
