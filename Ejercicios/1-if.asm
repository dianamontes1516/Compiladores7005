.data                   # *** SEGMENTO DE DATOS ***
false:		.word 0 	# Dato que representa el valor de Falso

#---------------------------------------------------------------------------
.text                   # *** SEGMENTO DE CÓDIGO ***
main:                           # Rutina principal

## IF
        lw $s1 false
	beq $s1 $zero else1 	# Comparo: si el valor de la condición es Falso, entonces se hace el else. De otro modo se continua con el then.
## Then
	li $v0 1
	b fin_if1
## Else
else1 : li $v0 2

fin_if1:

## If
        lw $s1 false
        beq $s1 $zero else2
## Then
	li $v1 3
	b fin_if2
## Else
else2 : li $v1 4

fin_if2:
end: 	jr $ra
